package qarea;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.qaarea.ArticlesPage;
import com.qaarea.HomePage;
import com.qaarea.QareaCom;
import com.qaarea.tools.WebApp;
import com.qaarea.tools.WebAppTest;

public class TestTask2 implements WebAppTest {
	
	QareaCom qareacom = new QareaCom();
	private HomePage homepage;
	private ArticlesPage articlespage;
	
	@Test
	public void TestTask2() {
		homepage = qareacom.openHomePage();
		homepage.showPressCenterDropdown();
		articlespage = homepage.goToArticles();
		articlespage.openTheLatestArticle();
		
		
		String actualResult = articlespage.findShareBtn();
		String expectedResult = "SHARE";
		Assert.assertEquals(actualResult, expectedResult);
	}

	@Override
	public WebApp getTestedApp() {
		// TODO Auto-generated method stub
		return null;
	}

}