package qarea;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.qaarea.HomePage;
import com.qaarea.ProjectsPage;
import com.qaarea.QareaCom;
import com.qaarea.tools.WebApp;
import com.qaarea.tools.WebAppTest;

public class TestTask1 implements WebAppTest {
	
	QareaCom qareacom = new QareaCom();
	private HomePage homepage;
	private ProjectsPage projectspage;
	
	@Test
	public void TestTask1() {
		homepage = qareacom.openHomePage();
		homepage.showProjectsSuccessDropdown();
		projectspage = homepage.goToProjects();
		projectspage.goToTestingAndQAsection();
		projectspage.goToNextPage();
		
		String actualResult = projectspage.findFirstPageBtn();
		String expectedResult = "first";
		
		Assert.assertEquals(actualResult, expectedResult);
	}

	@Override
	public WebApp getTestedApp() {
		// TODO Auto-generated method stub
		return null;
	}

}
