package com.qaarea.tools;

public interface WebApp {
	public byte[] takeScreenshot(String screenshotName);
}
