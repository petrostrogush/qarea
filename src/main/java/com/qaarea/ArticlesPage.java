package com.qaarea;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ArticlesPage {

	@FindBy(css = "#blog-posts > article:nth-child(1) > div.article-info.col-xs-12 > h3 > a")
	private WebElement latestArticle;
	
	@FindBy(css = "body > div.wrapper > div > div > section > div > div > div > div > div.share-box > p")
	private WebElement shareBtn;
	
	private WebDriver driver;

	public ArticlesPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void openTheLatestArticle() {
		latestArticle.click();
	}
	
	public String findShareBtn() {
		String share = shareBtn.getText();
		return share;
	}
}
