package com.qaarea;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	
	@FindBy(xpath = "//li[contains (.,'Projects Success')]")
	private WebElement projectSuccessTab;
	
	@FindBy(xpath = "//li[contains (.,'Press Center')]")
	private WebElement pressCenterTab;
	
	@FindBy(xpath = "//*[@id='navigation']/li[6]/ul/li[1]")
	private WebElement projectsBtn;
	
	@FindBy(xpath = "//*[@id='navigation']/li[7]/ul/li[3]/a")
	private WebElement articlesBtn;

	private WebDriver driver;

	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void showProjectsSuccessDropdown() {
	Actions actions = new Actions(driver);
	actions.moveToElement(projectSuccessTab).build().perform();
	}
	
	public ProjectsPage goToProjects() {
		projectsBtn.click();
		return new ProjectsPage(driver);
	}
	
	public void showPressCenterDropdown() {
	Actions actions = new Actions(driver);
	actions.moveToElement(pressCenterTab).build().perform();
	}
	
	public ArticlesPage goToArticles() {
		articlesBtn.click();
		return new ArticlesPage(driver);
	}

}
