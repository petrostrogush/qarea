package com.qaarea;

import org.openqa.selenium.WebDriver;

import com.qaarea.tools.Browser;

import io.qameta.allure.Step;

public class QareaCom {
	
	private static final String LOGIN_PAGE_URL = "https://qarea.com/";

	private WebDriver driver;

	public QareaCom() {
	}

	@Step("Open Login page by URL: " + LOGIN_PAGE_URL)
	public HomePage openHomePage() {
		driver = Browser.open();
		driver.get(LOGIN_PAGE_URL);
		return new HomePage(driver);
	}

	@Step("Close application/browser")
	public void close() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}

}
