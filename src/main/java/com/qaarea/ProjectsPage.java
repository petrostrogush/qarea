package com.qaarea;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProjectsPage {

	@FindBy(css = "#filters > li:nth-child(5)")
	private WebElement testingAndQABtn;
	
	@FindBy(css = ".pager-next")
	private WebElement nextPagerBtn;
	
	@FindBy(css = ".pager-first.first")
	private WebElement firstPageBtn;

	private WebDriver driver;

	public ProjectsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void goToTestingAndQAsection() {
		testingAndQABtn.click();
	}
	
	public void goToNextPage() {
		nextPagerBtn.click();
	}
	public String findFirstPageBtn() {
		String first = firstPageBtn.getText();
		return first;
	}
}
